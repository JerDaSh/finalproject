﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MikudApp_001.Startup))]
namespace MikudApp_001
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
