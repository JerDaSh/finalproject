﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MikudApp_001.Models;
using MikudApp_001.ViewModel;

namespace MikudApp_001.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<ContactViewModel> contacts = new List<ContactViewModel>();
            //here MyContactBookEntities is our datacontext
            using (MikudDBEntities dc = new MikudDBEntities())
            {
                var v = (from a in dc.Guards
                         join b in dc.SPEs on a.ID equals b.GuardID
                         join c in dc.Shifts on a.ID equals c.GuardID
                         join d in dc.Standards on c.ShiftID equals d.ID
                         select new ContactViewModel
                         {
                              ID = a.ID,
                              FirstName = a.FirstName,
                              LastName = a.LastName,
                              Address = a.Address,
                              AgreeToWorkOnSaturday = a.AgreeToWorkOnSaturday,
                              Birthday = a.Birthday,
                              HaveACar = a.HaveACar,
                              Role = a.Role,
                              StartWorkingDay = a.StartWorkingDate,
                              NeedTraining = a.NeedTraining,
                              NextTrainingDate = a.NextTrainingDate,
                              TrainingFrequency = a.TrainingFriquencyInMounth
                         }).ToList();
                contacts = v;
            }
            return View(contacts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ListResult()
        {
            ViewBag.Message = "List";

            return View();
        }
    }
}