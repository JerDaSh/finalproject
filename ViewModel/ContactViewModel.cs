﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MikudApp_001.ViewModel
{
    public class ContactViewModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public bool AgreeToWorkOnSaturday { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? StartWorkingDay { get; set; }
        public string Role { get; set; }
        public bool HaveACar { get; set; }
        public DateTime? NextTrainingDate { get; set; }
        public bool NeedTraining { get; set; }
        public int? TrainingFrequency { get; set; }

    }
}